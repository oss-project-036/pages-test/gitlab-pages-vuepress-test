module.exports = {
	base: '/gitlab-pages-vuepress/',
	title: 'VuePress in Gitlab Pages',
	description: 'Static website built with VuePress and deployed on Gitlab Pages',
	dest: './public'
};
